Kotori - simple speedmeter app
==========

This project was a fork of [Speedmeter](https://github.com/flyingrub/SpeedMeter) keeping the core features:

- Actual, real-time speed
- Time elapsed
- Meters or kilometers (or miles) traveled
- Maximum speed and average speed

but now project added new some features like:

- Show latitude and longitude
- Optionally use MSL for altitude on android nougat or above
- Dark theme
- Speed and distance in notification
- Continue running in background

the code was rewrited using kotlin and AppCompat library was replaced with AndroidX (Jetpack).

## Permissions & Privacy
Fine and coarse location permissions are required to get data from device location hardware, **Kotori does not share this data with
anyone**.

<a href="https://f-droid.org/packages/dev.melonpan.kotori" target="_blank">
<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="75"/></a>


![](https://i.imgur.com/1kPQnKi.png)
