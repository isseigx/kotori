package dev.melonpan.kotori.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences

import java.io.IOException

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class UserPreferencesRepo(private val dataStore: DataStore<Preferences>) {
    private companion object {
        val AUTO_AVERAGE = booleanPreferencesKey("auto_average")
        val IMPERIAL = booleanPreferencesKey("imperial")
        val ALTITUDE_MSL = booleanPreferencesKey("altitude_msl")
    }

    val autoAverage: Flow<Boolean> = dataStore.data
    .catch { exception ->
        if (exception is IOException) {
            emit(emptyPreferences())
        }
        else {
            throw exception
        }
    }.map { preferences ->
        preferences[AUTO_AVERAGE] ?: false
    }

    val imperialUnits: Flow<Boolean> = dataStore.data
    .catch { exception ->
        if (exception is IOException) {
            emit(emptyPreferences())
        }
        else {
            throw exception
        }
    }.map { preferences ->
        preferences[IMPERIAL] ?: false
    }

    val altitudeMsl: Flow<Boolean> = dataStore.data
    .catch { exception ->
        if (exception is IOException) {
            emit(emptyPreferences())
        }
        else {
            throw exception
        }
    }.map { preferences ->
        preferences[ALTITUDE_MSL] ?: false
    }

    suspend fun enableAutoAverage(enable: Boolean) {
        dataStore.edit { preferences ->
            preferences[AUTO_AVERAGE] = enable
        }
    }

    suspend fun enableImperialUnits(enable: Boolean) {
        dataStore.edit { preferences ->
            preferences[IMPERIAL] = enable
        }
    }

    suspend fun enableAltitudeMsl(enable: Boolean) {
        dataStore.edit { preferences ->
            preferences[ALTITUDE_MSL] = enable
        }
    }
}
