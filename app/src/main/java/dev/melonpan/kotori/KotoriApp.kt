package dev.melonpan.kotori

import android.app.Application
import android.content.Context
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.preferencesDataStore

import dev.melonpan.kotori.data.UserPreferencesRepo

private const val USER_PREFERENCES_NAME = "pref_general"

private val Context.dataStore by preferencesDataStore(
    name = USER_PREFERENCES_NAME,
    produceMigrations = { context ->
        listOf(SharedPreferencesMigration(context, USER_PREFERENCES_NAME))
    }
)

class KotoriApp : Application() {
    lateinit var userPreferences: UserPreferencesRepo

    override fun onCreate() {
        super.onCreate()
        userPreferences = UserPreferencesRepo(dataStore)
    }
}
